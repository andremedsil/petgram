//
//  ContentViewModel.swift
//  petgram
//
//  Created by André Medeiros da Silva on 10/01/22.
//

import Foundation
import SwiftUI

final class ContentViewModel: ObservableObject {
    @Published private(set) var state: HomeViewModel.Response.ContentViewModelState
    @Published private(set) var properties: HomeViewModel.Response.ContentViewProperties
    
    init(inicialState: HomeViewModel.Response.ContentViewModelState = .init(posts: []), inicialProperties: HomeViewModel.Response.ContentViewProperties = .init()) {
        self.state = inicialState
        self.properties = inicialProperties
    }
    
    private func parse(jsonData: Data) {
        do {
            let data = try JSONDecoder().decode(HomeViewModel.Response.ContentViewModelState.self, from: jsonData)
            self.state = data
        } catch {
            print("decode error")
        }
    }
    
    func getList() {
        properties.isLoading = true
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(4)) {
            let worker = HomeService()
            worker.fetch(success: { (data) in
                self.parse(jsonData: data)
                self.properties.isLoading = false
            }, failure: { ( error ) in
                print(error.localizedDescription)
            })
            
        }
    }
}
