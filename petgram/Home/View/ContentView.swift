//
//  ContentView.swift
//  petgram
//
//  Created by André Medeiros da Silva on 07/01/22.
//

import SwiftUI
import Foundation

struct ContentView: View {
    @ObservedObject private var model: ContentViewModel
    
    init(model: ContentViewModel = .init()) {
        self.model = model 
        UINavigationBar.appearance().isTranslucent = true
        UINavigationBar.appearance().barTintColor = .black
    }
    
    var body: some View {
        ZStack {
            Color.black
                .ignoresSafeArea()
                .onAppear(perform: model.getList)
            Text(model.properties.isLoading ? "Carregando..." : String())
                .foregroundColor(.white)
            List(model.state.posts, id: \.user) { post in
                UserProfile(image: post.user, name: post.name, date: post.time)
                    .listRowBackground(Color.black)
                Image(post.image)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .cornerRadius(20)
                    .background(.black)
                    .listRowBackground(Color.black)
                    .padding(.bottom)
                PostFooter()
                    .listRowBackground(Color.black)
            }
            .listStyle(PlainListStyle())
            Spacer()
        }
        
        .navigationBarItems(leading: MainHeader(notifications: model.state.notifications))
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            ContentView(model: ContentViewModel(inicialState: .init(posts: [])))
        }
        .previewDisplayName("HOME")
    }
}
