//
//  HomeViewModel.swift
//  petgram
//
//  Created by André Medeiros da Silva on 10/01/22.
//

import Foundation

enum HomeViewModel {
    struct Response {
        struct ContentViewProperties {
            var isLoading: Bool = false
        }
        
        struct ContentViewModelState: Codable {
            var notifications: Int = 0
            var posts: [Post]
            
            struct Post: Codable {
                var user: String = String()
                var name: String = String()
                var time: String = String()
                var image: String = String()
                var description: String = String()
            }
        }
    }
}
