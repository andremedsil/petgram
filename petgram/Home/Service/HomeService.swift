//
//  HomeService.swift
//  petgram
//
//  Created by André Medeiros da Silva on 09/01/22.
//

import Foundation

class HomeService {
    typealias Success = ((_ response: Data) -> Void)
    typealias Fail = ((_ errorMessage: Error) -> Void)
   
    func fetch(success: @escaping Success, failure: @escaping Fail) {
        let name = "home"
        do {
            if let bundlePath = Bundle.main.path(forResource: name, ofType: "json"),
               let jsonData = try String(contentsOfFile: bundlePath).data(using: .utf8) {
               success(jsonData)
            }
        } catch {
            failure(error)
        }
    }
}

