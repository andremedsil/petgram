//
//  LoginView.swift
//  petgram
//
//  Created by André Medeiros da Silva on 08/01/22.
//

import SwiftUI

struct LoginView: View {
    
    @ObservedObject private var viewModel: LoginViewViewModel
    
    init(model: LoginViewViewModel) {
        self.viewModel = model
        UITableView.appearance().backgroundColor = .black
        UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: UIColor.white]
    }
    
    var body: some View {
        VStack {
            Image("nome")
                .resizable()
                .aspectRatio(contentMode: .fit)
            Image("logo")
                .resizable()
                .aspectRatio(contentMode: .fit)
            Form {
                Section  {
                    TextField("email", text: viewModel.bindings.email)
                        .keyboardType(.emailAddress)
                        .autocapitalization(.none)
                    SecureField("senha", text: viewModel.bindings.password)
                        .autocapitalization(.none)
                } footer: {
                    forgetMyPassword
                        .frame(width: 300, height: 20, alignment: .center)
                    Spacer()
                }
            }
        }
        .alert(isPresented: viewModel.bindings.loginError) {
            Alert(
                title: Text("Erro de Login"),
                message: Text("tente mais tarde")
            )
        }
        .background(.black)
        .navigationBarItems(trailing: submit)
        .navigationBarTitleDisplayMode(.inline)
    }
    
    private var submit: some View {
        Button(action: viewModel.login) {
            Text("Entrar")
                .font(.title2)
                .disabled(viewModel.state.canSubmit == false || viewModel.state.isLoading == true)
        }
    }
    
    private var forgetMyPassword: some View {
        Button(action: viewModel.login) {
            Text(viewModel.state.isLoading ? "Carregando..." : "Esqueci minha senha")
                .font(.body)
                .disabled(viewModel.state.isLoading == true)
        }
        .multilineTextAlignment(.center)
    }
    
    struct LoginViewState {
        var email: String = ""
        var password: String = ""
        var loginError: Bool = false
        var isLoading: Bool = false
        var canSubmit: Bool { email.isEmpty == false && password.isEmpty == false }
    }
    
    final class LoginViewViewModel: ObservableObject {
        @Published private(set) var state: LoginViewState = .init()
        
        init(inicialState: LoginViewState) {
            state = inicialState
        }
        
        var bindings: (email: Binding<String>, password: Binding<String>, loginError: Binding<Bool>, isLoading: Binding<Bool>)
        {
            (
                email: Binding(to: \.state.email, on: self),
                password: Binding(to: \.state.password, on: self),
                loginError: Binding(to: \.state.loginError, on: self),
                isLoading: Binding(to: \.state.isLoading, on: self)
            )
        }
        
        func login() {
            state.isLoading = true
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3)) {
                self.state.loginError = true
                self.state.isLoading = false
            }
        }
    }
}

extension Binding {
    init<ObjectType: AnyObject> (to path: ReferenceWritableKeyPath<ObjectType, Value>, on object: ObjectType) {
        self.init {
            object[keyPath: path]
        } set: {
            object[keyPath: path] = $0
        }
    }
}

struct Login_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            LoginView(model: .init(inicialState: .init()))
        }
    }
}
