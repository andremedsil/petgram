//
//  MainHeader.swift
//  petgram
//
//  Created by André Medeiros da Silva on 09/01/22.
//

import SwiftUI

public struct MainHeader: View {

    private(set) var notifications: Int
    
    init(notifications: Int) {
        self.notifications = notifications
    }
    
    public var body: some View {
        VStack {
            HStack {
                Image("nome")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                Spacer()
                
                ZStack {
                    Image(uiImage: UIImage(systemName: "bell") ?? UIImage())
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 30, height: 30)
                        .colorInvert()
                        .padding(.init(top: 0, leading: 0, bottom: 0, trailing: 10))
                    if notifications != 0 {
                        Group {
                            NotificationCircle(notifications: notifications)
                                .padding(.bottom)
                                .padding(.leading)
                        }
                    }
                }
          
                Image(uiImage: UIImage(systemName: "magnifyingglass") ?? UIImage())
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 30, height: 30)
                    .padding(.init(top: 0, leading: 0, bottom: 0, trailing: 10))
                    .colorInvert()
                Image(uiImage: UIImage(systemName: "camera.on.rectangle") ?? UIImage())
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 30, height: 30)
                    .colorInvert()
          
               
            }
            Divider()
                .background(.white)
        }
    }
}

struct MainHeader_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView{
            ZStack {
                Color.black
                    .ignoresSafeArea()
                    .navigationBarItems(leading: MainHeader(notifications: 20))
                
            }  
        }
    }
}
