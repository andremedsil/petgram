//
//  UserProfile.swift
//  petgram
//
//  Created by André Medeiros da Silva on 10/01/22.
//

import SwiftUI

struct UserProfile: View {
    
    private(set) var image: String
    private(set) var name: String
    private(set) var date: String
    
    init(image: String, name: String, date: String?) {
        self.image = image
        self.name = name
        self.date = date ?? String()
    }
    
    var body: some View {
        HStack {
            Image(image)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .clipShape(Circle())
                .shadow(radius: 10)
                .overlay(Circle().stroke(Color.purple, lineWidth: 3))
                .frame(width: 50, height: 50)
            
            VStack(alignment: .leading) {
                Text(name)
                    .bold()
                    .foregroundColor(.white)
                if !date.isEmpty {
                    Group {
                        Text(date)
                            .bold()
                            .font(.footnote)
                            .foregroundColor(.white)
                    }
                }
            }
            Spacer()
        }
    }
}

struct UserProfile_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Color.black
                .ignoresSafeArea()
            UserProfile(image: "dog2", name: "ndmedeiros", date: "2 Minutos atrás")
        }
        
    }
}
