//
//  NotificationCircle.swift
//  petgram
//
//  Created by André Medeiros da Silva on 09/01/22.
//

import SwiftUI

public struct NotificationCircle: View {

    private(set) var notifications: Int
    
    init(notifications: Int) {
        self.notifications = notifications
    }
    
    public var body: some View {
        ZStack {
            Text(String(notifications))
                .foregroundColor(.white)
                .font(.custom("notification", size: 15))
                .bold()
                .padding(5)
        }
        .background(.red)
        .cornerRadius(50)
        
    }
    
    struct NotificationCircle_Previews: PreviewProvider {
        static var previews: some View {
            ZStack {
                Color.black
                    .ignoresSafeArea()
                NotificationCircle(notifications: 12)
            }
        }
    }
}
