//
//  PostFooter.swift
//  petgram
//
//  Created by André Medeiros da Silva on 10/01/22.
//

import SwiftUI

struct PostFooter: View {
    var body: some View {
        VStack {
            HStack {
                Image(uiImage: UIImage(systemName: "heart") ?? UIImage())
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 30, height: 30)
                    .padding(.init(top: 0, leading: 0, bottom: 0, trailing: 10))
                    .colorInvert()
                Text("Curtir")
                    .foregroundColor(.white)
                Spacer()
                Image(uiImage: UIImage(systemName: "bubble.right") ?? UIImage())
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 30, height: 30)
                    .colorInvert()
                Text("Comentar")
                    .foregroundColor(.white)
                Spacer()
                Image(uiImage: UIImage(systemName: "paperplane") ?? UIImage())
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 30, height: 30)
                    .colorInvert()
                Text("Compartilhar")
                    .foregroundColor(.white)
            }
            Divider()
                .background(.white)
        }
    }
}

struct PostFooter_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Color.black
                .ignoresSafeArea()
                PostFooter()
        }
    }
}
