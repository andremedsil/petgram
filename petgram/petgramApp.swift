//
//  petgramApp.swift
//  petgram
//
//  Created by André Medeiros da Silva on 07/01/22.
//

import SwiftUI

@main
struct petgramApp: App {
    var body: some Scene {
        WindowGroup {
            NavigationView {
                ContentView()
            }
           // ContentView(model: ContentViewModel(inicialState: .init()))
        }
    }
}
